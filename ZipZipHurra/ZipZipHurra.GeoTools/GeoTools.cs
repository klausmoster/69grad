﻿using System.Collections.Generic;
using System.Linq;
using ZipZipHurra.DataAndContracts.Contracts;
using ZipZipHurra.DataAndContracts.Models;
using ZipZipHurra.GeoTools.ExtensionMethods;

namespace ZipZipHurra.GeoTools
{
    public class GeoTools : IGeoTools
    {
        /// <summary>
        /// Liefert die n Positionen in der Nähe.
        /// </summary>
        /// <param name="referencePosition">Ausgangs-Position.</param>
        /// <param name="comparePositions">Vergleichs-Positionen.</param>
        /// <param name="count">Anzahl Positionen.</param>
        /// <returns>Die Positionen in der Nähe.</returns>
        public List<City> GetNearestCities(City referencePosition, List<City> comparePositions, int count)
        {
            List<City> nearestPositionIds = (from p in comparePositions
                                               select new
                                               {
                                                   City = p,
                                                   Distance = referencePosition.GeoCoordinate.DistanceTo(p.GeoCoordinate)
                                               }).OrderBy(d => d.Distance).Select(d => d.City).Take(count).ToList();

            return nearestPositionIds;
        }
    }
}
