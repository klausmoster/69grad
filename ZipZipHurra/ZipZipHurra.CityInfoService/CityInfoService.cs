﻿using System.Globalization;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ZipZipHurra.DataAndContracts.Contracts;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.CityInfoService
{
    public class CityInfoService : ICityInfoService
    {
        private readonly INetworkCommunicator _networkCommunicator;

        public CityInfoService(INetworkCommunicator networkCommunicator)
        {
            _networkCommunicator = networkCommunicator;
        }

        /// <summary>
        /// Ermittelt die Stadt-Informationen anhand des Wikipedia-Stadt-Quelltextes.
        /// </summary>
        /// <param name="city">Der Name der Stadt.</param>
        /// <returns>Die Stadtinformationen.</returns>
        public async Task<CityInformation> GetInformation(string city)
        {
            string wikiPage = $"https://de.wikipedia.org/wiki/{city}";
            string html = await _networkCommunicator.DownloadString(wikiPage).ConfigureAwait(false);

            html = WebUtility.HtmlDecode(html);
            html = Regex.Replace(html, @"line-height:[^;]+;", "", RegexOptions.IgnoreCase);
            html = HtmlRemoval.StripTagsRegex(html);
            CityInformation cityInformation = new CityInformation();

            Regex regex = new Regex(@"Einwohner:[^\d]*(?<einwohner>(\d|\.)*)", RegexOptions.Singleline);
            Match match = regex.Match(html);
            if (match.Success)
            {
                cityInformation.Population = double.Parse(match.Groups["einwohner"].Value.Replace(".", ""), CultureInfo.InvariantCulture);
            }

            return cityInformation;
        }
    }
}