﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using ZipZipHurra.DataAndContracts.Contracts;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.TabFileReader
{
    public class TabFileReader : ITabFileReader
    {
        /// <summary>
        /// Erstellt die nach Stadt gruppierte Liste aller Einträge.
        /// </summary>
        /// <param name="stream">Der Stream der Tab-getrennten Datei.</param>
        /// <returns>Die gruppierte Liste.</returns>
        public Dictionary<string, List<City>> ReadCityDefinitions(Stream stream)
        {
            Dictionary<string, List<City>> cityDefinitions = new Dictionary<string, List<City>>();

            using (StreamReader streamReader = new StreamReader(stream))
            {
                while (streamReader.Peek() >= 0)
                {
                    string nextLine = streamReader.ReadLine();
                    if (nextLine.StartsWith("#"))
                    {
                        continue;
                    }

                    string[] splittedRow = nextLine.Split('\t');
                    if (splittedRow.Length == 5)
                    {
                        City city = new City
                        {
                            Id = int.Parse(splittedRow[0]),
                            Zip = int.Parse(splittedRow[1]),
                            GeoCoordinate = new GeoCoordinate
                            {
                                Longitude = double.Parse(splittedRow[2], CultureInfo.InvariantCulture),
                                Latitude = double.Parse(splittedRow[3], CultureInfo.InvariantCulture),
                            },
                            Name = splittedRow[4]
                        };

                        if (!cityDefinitions.ContainsKey(city.Name))
                        {
                            cityDefinitions.Add(city.Name, new List<City>());
                        }

                        cityDefinitions[city.Name].Add(city);
                    }
                }
            }

            return cityDefinitions;
        }
    }
}
