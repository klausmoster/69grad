﻿namespace ZipZipHurra.DataAndContracts.Models
{
    public class CityInformation
    {
        /// <summary>
        /// Die Einwohnerzahl.
        /// </summary>
        public double Population { get; set; }
    }
}
