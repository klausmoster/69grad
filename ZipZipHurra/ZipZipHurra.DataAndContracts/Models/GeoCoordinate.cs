﻿namespace ZipZipHurra.DataAndContracts.Models
{
    public class GeoCoordinate
    {
        /// <summary>
        /// Der Breitengrad.
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Der Längengrad.
        /// </summary>
        public double Longitude { get; set; }
    }
}
