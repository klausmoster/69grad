﻿namespace ZipZipHurra.DataAndContracts.Models
{
    public class City
    {
        /// <summary>
        /// Die Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Der Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Die PLZ.
        /// </summary>
        public int Zip { get; set; }

        /// <summary>
        /// Die Geokoordinaten.
        /// </summary>
        public GeoCoordinate GeoCoordinate { get; set; }
    }
}
