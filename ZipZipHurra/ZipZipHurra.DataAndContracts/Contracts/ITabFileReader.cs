﻿using System.Collections.Generic;
using System.IO;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.DataAndContracts.Contracts
{
    public interface ITabFileReader
    {
        /// <summary>
        /// Erstellt die nach Stadt gruppierte Liste aller Einträge.
        /// </summary>
        /// <param name="stream">Der Stream der Tab-getrennten Datei.</param>
        /// <returns>Die gruppierte Liste.</returns>
        Dictionary<string, List<City>> ReadCityDefinitions(Stream stream);
    }
}
