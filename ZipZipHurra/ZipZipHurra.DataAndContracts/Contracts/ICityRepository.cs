﻿using System.Collections.Generic;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.DataAndContracts.Contracts
{
    public interface ICityRepository
    {
        /// <summary>
        /// Auflistung aller Städte mit dazugehörigen PLZ.
        /// </summary>
        Dictionary<string, List<City>> GetAll();

        /// <summary>
        /// Ermittelt die n nächstgelegenen Städte bzw. Stadtteile.
        /// </summary>
        /// <param name="city">Die Referenzstadt.</param>
        /// <param name="count">Die Anzahl der weiteren PLZ.</param>
        List<City> GetNearestFrom(City city, int count);
    }
}
