﻿using System.Threading.Tasks;

namespace ZipZipHurra.DataAndContracts.Contracts
{
    public interface INetworkCommunicator
    {
        /// <summary>
        /// Lädt den Content der Url.
        /// </summary>
        /// <param name="url">Die Url.</param>
        /// <returns>Der Content.</returns>
        Task<string> DownloadString(string url);
    }
}
