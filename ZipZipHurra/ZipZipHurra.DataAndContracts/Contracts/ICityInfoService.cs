﻿using System.Threading.Tasks;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.DataAndContracts.Contracts
{
    public interface ICityInfoService
    {
        /// <summary>
        /// Ermittelt die Stadt-Informationen anhand des Wikipedia-Stadt-Quelltextes.
        /// </summary>
        /// <param name="city">Der Name der Stadt.</param>
        /// <returns>Die Stadtinformationen.</returns>
        Task<CityInformation> GetInformation(string city);
    }
}
