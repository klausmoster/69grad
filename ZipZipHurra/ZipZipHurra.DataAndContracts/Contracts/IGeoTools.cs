﻿using System.Collections.Generic;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.DataAndContracts.Contracts
{
    public interface IGeoTools
    {
        /// <summary>
        /// Liefert die n Positionen in der Nähe.
        /// </summary>
        /// <param name="referencePosition">Ausgangs-Position.</param>
        /// <param name="comparePositions">Vergleichs-Positionen.</param>
        /// <param name="count">Anzahl Positionen.</param>
        /// <returns>Die Positionen in der Nähe.</returns>
        List<City> GetNearestCities(City referencePosition, List<City> comparePositions, int count);
    }
}
