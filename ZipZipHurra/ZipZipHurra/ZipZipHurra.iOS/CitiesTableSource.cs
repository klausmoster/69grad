﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using Microsoft.Practices.Unity;
using UIKit;
using ZipZipHurra.DataAndContracts.Contracts;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.iOS
{
    public class CitiesTableSource : UITableViewSource
    {
        private readonly UITableViewController _parent;
        private readonly Dictionary<string, List<City>> _cities;
        string CellIdentifier = "TableCell";
        private Dictionary<string, List<string>> _indexedTableItems;
        private string[] _indexKeys;
        private readonly ICityRepository _cityRepository;

        public CitiesTableSource(UITableViewController parent)
        {
            _cityRepository = AppConfig.AppConfig.Configuration.Resolve<ICityRepository>();
            _cities = _cityRepository.GetAll();
            _parent = parent;

            _indexedTableItems = new Dictionary<string, List<string>>();
            foreach (var t in _cities)
            {
                if (_indexedTableItems.ContainsKey(t.Key[0].ToString()))
                {
                    _indexedTableItems[t.Key[0].ToString()].Add(t.Key);
                }
                else
                {
                    _indexedTableItems.Add(t.Key[0].ToString(), new List<string>() { t.Key });
                }
            }
            _indexKeys = _indexedTableItems.Keys.ToArray();
        }

        public override string TitleForHeader(UITableView tableView, nint section)
        {
            nint rowsInSection = RowsInSection(tableView, section);
            if (rowsInSection > 1)
            {
                return $"{_indexKeys[section]} ({rowsInSection})";
            }
            else
            {
                return _indexKeys[section];
            }
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _indexedTableItems[_indexKeys[section]].Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell(CellIdentifier);
            string key = _indexedTableItems[_indexKeys[indexPath.Section]][indexPath.Row];
            
            List<City> cities = _cities[key];

            //---- if there are no cells to reuse, create a new one
            if (cell == null)
            {
                cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier)
                {
                    Accessory = UITableViewCellAccessory.DisclosureIndicator
                };
            }

            if (cities.Count > 1)
            {
                cell.TextLabel.Text = $"{cities.First().Name} ({cities.Count})";
            }
            else
            {
                cell.TextLabel.Text = cities.First().Name;
            }
            

            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
            
            // TODO: Als Event auslagern, MemoryLeak?

            string key = _indexedTableItems[_indexKeys[indexPath.Section]][indexPath.Row];
            if (_cities.ContainsKey(key))
            {
                List<City> cities = _cities[key];
                if (cities.Count > 1)
                {
                    ZipViewController zipViewController =
                        _parent.Storyboard.InstantiateViewController("ZipView") as ZipViewController;
                    if (zipViewController != null)
                    {
                        zipViewController.SetCities(cities);
                        _parent.NavigationController.PushViewController(zipViewController, true);
                    }
                }
                else
                {
                    ZipDetailsViewController zipDetailsViewController = _parent.Storyboard.InstantiateViewController("ZipDetailsView") as ZipDetailsViewController;
                    if (zipDetailsViewController != null)
                    {
                        zipDetailsViewController.SetCity(cities.First());
                        _parent.NavigationController.PushViewController(zipDetailsViewController, true);
                    }
                }
            }
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return _indexKeys.Length;
        }
        public override string[] SectionIndexTitles(UITableView tableView)
        {
            return _indexKeys;
        }
    }
}
