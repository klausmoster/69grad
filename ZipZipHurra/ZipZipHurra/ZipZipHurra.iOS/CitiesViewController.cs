﻿using System;
using Microsoft.Practices.Unity;
using UIKit;

namespace ZipZipHurra.iOS
{
    public partial class CitiesViewController : UITableViewController
    {
        public CitiesViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            InitData();
        }

        private void InitData()
        {
            UITableView uiTableView = new UITableView(View.Bounds, UITableViewStyle.Grouped)
            {
                Source = new CitiesTableSource(this)
            };
            Add(uiTableView);
        }
    }
}

