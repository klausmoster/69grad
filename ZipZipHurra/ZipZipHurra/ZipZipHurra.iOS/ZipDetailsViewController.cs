﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CoreLocation;
using Microsoft.Practices.Unity;
using UIKit;
using ZipZipHurra.DataAndContracts.Contracts;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.iOS
{
    public partial class ZipDetailsViewController : UIViewController
    {
        private City _city;
        private readonly ICityInfoService _cityInfoService;
        private readonly ICityRepository _cityRepository;

        public ZipDetailsViewController(IntPtr handle) : base(handle)
        {
            _cityInfoService = AppConfig.AppConfig.Configuration.Resolve<ICityInfoService>();
            _cityRepository = AppConfig.AppConfig.Configuration.Resolve<ICityRepository>();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            LoadDetails();
        }

        public void SetCity(City selectedCity)
        {
            _city = selectedCity;
        }

        private void LoadDetails()
        {
            Title = _city.Zip.ToString();
            Latitude.Text = _city.GeoCoordinate.Latitude.ToString(CultureInfo.InvariantCulture);
            Longitude.Text = _city.GeoCoordinate.Longitude.ToString(CultureInfo.InvariantCulture);

            List<City> nearestCities = _cityRepository.GetNearestFrom(_city, 10);
            ShowNearestCities(nearestCities);

            _cityInfoService.GetInformation(_city.Name).ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    Population.Text = $"{t.Result.Population:0,0}";
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void ShowNearestCities(List<City> nearestPositions)
        {
            BasicMapAnnotation[] mapAnnotations = nearestPositions.Select(p => new BasicMapAnnotation(
                new CLLocationCoordinate2D(p.GeoCoordinate.Latitude,
                    p.GeoCoordinate.Longitude), p.Zip.ToString(), "")).ToArray();

            ZipMap.ShowAnnotations(mapAnnotations, true);
        }
    }
}

