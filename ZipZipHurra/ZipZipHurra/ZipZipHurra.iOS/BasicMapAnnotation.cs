﻿using CoreLocation;
using MapKit;

namespace ZipZipHurra.iOS
{
    public class BasicMapAnnotation : MKAnnotation
    {
        CLLocationCoordinate2D _coord;
        private readonly string _title;
        private readonly string _subtitle;

        public override CLLocationCoordinate2D Coordinate
        {
            get { return _coord; }
        }

        public override void SetCoordinate(CLLocationCoordinate2D value)
        {
            _coord = value;
        }

        public override string Title
        {
            get { return _title; }
        }

        public override string Subtitle
        {
            get { return _subtitle; }
        }

        public BasicMapAnnotation(CLLocationCoordinate2D coordinate, string title, string subtitle)
        {
            _coord = coordinate;
            _title = title;
            _subtitle = subtitle;
        }
    }
}