﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.iOS
{
    public class ZipTableSource : UITableViewSource
    {
        private readonly UITableViewController _parent;
        private readonly List<City> _cities;
        string CellIdentifier = "TableCell";

        public ZipTableSource(List<City> items, UITableViewController parent)
        {
            _cities = items;
            _parent = parent;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _cities.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell(CellIdentifier);
            City city = _cities[indexPath.Row];

            //---- if there are no cells to reuse, create a new one
            if (cell == null)
            {
                cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier)
                {
                    Accessory = UITableViewCellAccessory.DisclosureIndicator
                };
            }
            cell.TextLabel.Text = city.Zip.ToString();

            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);

            // TODO: Als Event auslagern, MemoryLeak?

            City selectedCity = _cities[indexPath.Row];
            ZipDetailsViewController zipViewController = _parent.Storyboard.InstantiateViewController("ZipDetailsView") as ZipDetailsViewController;
            if (zipViewController != null)
            {
                zipViewController.SetCity(selectedCity);
            }
            _parent.NavigationController.PushViewController(zipViewController, true);
        }
    }
}
