// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ZipZipHurra.iOS
{
	[Register ("ZipViewController")]
	partial class ZipViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView ZipController { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ZipController != null) {
				ZipController.Dispose ();
				ZipController = null;
			}
		}
	}
}
