// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ZipZipHurra.iOS
{
	[Register ("CitiesViewController")]
	partial class CitiesViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView CitiesController { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (CitiesController != null) {
				CitiesController.Dispose ();
				CitiesController = null;
			}
		}
	}
}
