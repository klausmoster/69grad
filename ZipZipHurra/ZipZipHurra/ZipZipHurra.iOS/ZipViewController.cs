﻿using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.iOS
{
    public partial class ZipViewController : UITableViewController
    {
        private List<City> _cities;

        public ZipViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            InitData();
        }

        private void InitData()
        {
            Title = _cities.First().Name;

            UITableView uiTableView = new UITableView(View.Bounds) { Source = new ZipTableSource(_cities, this) };
            Add(uiTableView);
        }

        public void SetCities(List<City> cities)
        {
            _cities = cities;
        }
    }
}

