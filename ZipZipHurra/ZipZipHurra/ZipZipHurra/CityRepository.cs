﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ZipZipHurra.DataAndContracts.Contracts;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.DataService
{
    public sealed class CityRepository : ICityRepository
    {
        private readonly IGeoTools _geoTools;
        private readonly ITabFileReader _tabFileReader;
        private Dictionary<string, List<City>> _cities;

        public CityRepository(IGeoTools geoTools, ITabFileReader tabFileReader)
        {
            _geoTools = geoTools;
            _tabFileReader = tabFileReader;
        }

        /// <summary>
        /// Ermittelt alle Städte.
        /// </summary>
        /// <returns>Die Liste aller Städte.</returns>
        private Dictionary<string, List<City>> ReadAllCitiesFromTabFile()
        {
            Assembly assembly = typeof(CityRepository).GetTypeInfo().Assembly;
            string resourceName = "ZipZipHurra.DataService.Data.PLZ.tab";

            Dictionary<string, List<City>> cities;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                cities = _tabFileReader.ReadCityDefinitions(stream);
            }

            cities = cities.OrderBy(c => c.Key).ToDictionary(pair => pair.Key, pair => pair.Value);
            return cities;
        }

        /// <summary>
        /// Auflistung aller Städte mit dazugehörigen PLZ.
        /// </summary>
        public Dictionary<string, List<City>> GetAll()
        {
            if (_cities == null)
            {
                _cities = ReadAllCitiesFromTabFile();
            }

            return _cities;
        }

        /// <summary>
        /// Ermittelt die n nächstgelegenen Städte bzw. Stadtteile.
        /// </summary>
        /// <param name="city">Die Referenzstadt.</param>
        /// <param name="count">Die Anzahl der weiteren PLZ.</param>
        public List<City> GetNearestFrom(City city, int count)
        {
            List<City> allCities = GetAll().SelectMany(c => c.Value).Where(c => c.Zip != city.Zip).ToList();
            List<City> nearestCities = _geoTools.GetNearestCities(city, allCities, count);

            return nearestCities;
        }
    }
}