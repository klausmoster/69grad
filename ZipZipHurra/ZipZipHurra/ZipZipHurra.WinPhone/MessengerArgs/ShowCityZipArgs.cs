﻿using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.WinPhone.MessengerArgs
{
    public class ShowCityZipArgs
    {
        public City City { get; }

        public ShowCityZipArgs(City city)
        {
            City = city;
        }
    }
}
