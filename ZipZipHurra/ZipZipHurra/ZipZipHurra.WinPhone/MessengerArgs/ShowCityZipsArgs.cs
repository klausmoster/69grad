﻿using System.Collections.Generic;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.WinPhone.MessengerArgs
{
    public class ShowCityZipsArgs
    {
        public List<City> Cities { get; }

        public ShowCityZipsArgs(List<City> cities)
        {
            Cities = cities;
        }
    }
}
