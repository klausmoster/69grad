﻿using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.WinPhone.MessengerArgs
{
    public class SetMapLocationArgs
    {
        public GeoCoordinate GeoCoordinate { get; }

        public SetMapLocationArgs(GeoCoordinate geoCoordinate)
        {
            GeoCoordinate = geoCoordinate;
        }
    }
}
