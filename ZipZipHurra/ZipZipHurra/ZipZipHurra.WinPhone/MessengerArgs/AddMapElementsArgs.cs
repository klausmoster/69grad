﻿using System.Collections.Generic;
using ZipZipHurra.WinPhone.Models;

namespace ZipZipHurra.WinPhone.MessengerArgs
{
    public class AddMapElementsArgs
    {
        public List<MapElement> MapElements { get; set; }
        
        public AddMapElementsArgs(List<MapElement> mapElements)
        {
            MapElements = mapElements;
        }
    }
}
