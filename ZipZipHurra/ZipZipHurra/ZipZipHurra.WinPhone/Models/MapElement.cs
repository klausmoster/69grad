﻿using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.WinPhone.Models
{
    public class MapElement
    {
        /// <summary>
        /// Die Bezeichnung.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Die Geoposition.
        /// </summary>
        public GeoCoordinate GeoCoordinate { get; set; }
    }
}
