﻿namespace ZipZipHurra.WinPhone.Models
{
    public class CityView
    {
        public string Key { get; set; }

        public string Title { get; set; }
    }
}
