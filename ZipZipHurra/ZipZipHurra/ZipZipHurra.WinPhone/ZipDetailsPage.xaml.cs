﻿using System.Collections.Generic;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Messaging;
using ZipZipHurra.DataAndContracts.Models;
using ZipZipHurra.WinPhone.MessengerArgs;
using ZipZipHurra.WinPhone.Models;
using ZipZipHurra.WinPhone.ViewModel;
using MapElement = ZipZipHurra.WinPhone.Models.MapElement;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace ZipZipHurra.WinPhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ZipDetailsPage : BasePage
    {
        private readonly ZipDetailsViewModel _zipDetailsViewModel;

        public ZipDetailsPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;

            _zipDetailsViewModel = (ZipDetailsViewModel)DataContext;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Messenger.Default.Register<AddMapElementsArgs>(this, OnAddMapElementsArgs);
            Messenger.Default.Register<SetMapLocationArgs>(this, OnSetMapLocation);
            _zipDetailsViewModel.InitCommand.Execute(null);
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            Messenger.Default.Unregister(this);
        }

        private void OnAddMapElementsArgs(AddMapElementsArgs addMapElementsArgs)
        {
            ZipMapControl.MapElements.Clear();
            foreach (MapElement mapElement in addMapElementsArgs.MapElements)
            {
                AddMapIcon(mapElement);
            }
            FindBestZoomLevel(addMapElementsArgs.MapElements);
        }

        private void FindBestZoomLevel(List<MapElement> mapElements)
        {
            ZipMapControl.ZoomLevel = 20;
            while (!AreAllElementsInView(mapElements) && ZipMapControl.ZoomLevel >= 12)
            {
                ZipMapControl.ZoomLevel = ZipMapControl.ZoomLevel - 1;
            }
        }

        private bool AreAllElementsInView(List<MapElement> mapElements)
        {
            foreach (MapElement mapElement in mapElements)
            {
                Geopoint geopoint = new Geopoint(new BasicGeoposition
                {
                    Longitude = mapElement.GeoCoordinate.Longitude,
                    Latitude = mapElement.GeoCoordinate.Latitude
                });
                bool isInView;
                ZipMapControl.IsLocationInView(geopoint, out isInView);

                if (!isInView)
                {
                    return false;
                }
            }
            return true;
        }

        private void OnSetMapLocation(SetMapLocationArgs setMapLocationArgs)
        {
            SetMapLocation(setMapLocationArgs.GeoCoordinate);
        }

        private void AddMapIcon(MapElement mapElement)
        {
            MapIcon mapIcon = new MapIcon
            {
                Location = new Geopoint(new BasicGeoposition
                {
                    Latitude = mapElement.GeoCoordinate.Latitude,
                    Longitude = mapElement.GeoCoordinate.Longitude
                }),
                NormalizedAnchorPoint = new Point(0.5, 1.0),
                Title = mapElement.Title
            };
            ZipMapControl.MapElements.Add(mapIcon);
        }

        private void SetMapLocation(GeoCoordinate geoCoordinate)
        {
            Geopoint geopoint = new Geopoint(new BasicGeoposition
            {
                Latitude = geoCoordinate.Latitude,
                Longitude = geoCoordinate.Longitude
            });
            ZipMapControl.Center = geopoint;
        }
    }
}