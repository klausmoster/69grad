using System.Collections.Generic;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using QKit.JumpList;
using ZipZipHurra.DataAndContracts.Contracts;
using ZipZipHurra.DataAndContracts.Models;
using ZipZipHurra.WinPhone.Models;

namespace ZipZipHurra.WinPhone.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly ICityRepository _cityRepository;

        /// <summary>
        /// �bersicht aller St�dte.
        /// </summary>
        private Dictionary<string, List<City>> _cityDefinitions;

        private List<JumpListGroup<KeyValuePair<string, List<City>>>> _jumpListData;

        /// <summary>
        /// Ausgew�hlte Stadt.
        /// </summary>
        public List<JumpListGroup<KeyValuePair<string, List<City>>>> JumpListData
        {
            get
            {
                return _jumpListData;
            }

            set
            {
                if (_jumpListData == value)
                {
                    return;
                }

                _jumpListData = value;
                RaisePropertyChanged(nameof(JumpListData));
            }
        }

        /// <summary>
        /// �bersicht aller Stadtnamen.
        /// </summary>
        public ObservableCollection<CityView> Cities { get; set; } = new ObservableCollection<CityView>();

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
            InitData();
        }

        private void InitData()
        {
            if (IsInDesignMode)
            {
                _cityDefinitions = LoadDesignData();

            }
            else
            {
                _cityDefinitions = _cityRepository.GetAll();
            }

            Cities.Clear();
            foreach (KeyValuePair<string, List<City>> cityDefinition in _cityDefinitions)
            {
                CityView cityView = new CityView
                {
                    Key = cityDefinition.Key,
                    Title = $"{cityDefinition.Key} ({cityDefinition.Value.Count})"
                };
                Cities.Add(cityView);
            }

            JumpListData = _cityDefinitions.ToAlphaGroups(x => x.Key[0].ToString());
        }

        private Dictionary<string, List<City>> LoadDesignData()
        {
            Dictionary<string, List<City>> cityDefinitions = new Dictionary<string, List<City>>
                {
                    {"Aachen", new List<City>()},
                    {"Berlin", new List<City>()}
                };
            return cityDefinitions;
        }
    }
}