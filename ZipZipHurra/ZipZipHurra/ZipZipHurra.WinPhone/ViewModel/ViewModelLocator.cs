/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:ZipZipHurra.WinPhone"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using Microsoft.Practices.Unity;

namespace ZipZipHurra.WinPhone.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        private ZipSelectorViewModel _zipSelectorViewModel;
        private ZipDetailsViewModel _zipDetailsViewModel;
        private readonly UnityContainer _configuration;

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            _configuration = AppConfig.AppConfig.Configuration;

            _configuration.RegisterType<MainViewModel>();
            _configuration.RegisterType<ZipSelectorViewModel>();
            _configuration.RegisterType<ZipDetailsViewModel>();
            
            _zipSelectorViewModel = _configuration.Resolve<ZipSelectorViewModel>();
            _zipDetailsViewModel = _configuration.Resolve<ZipDetailsViewModel>();
        }

        public MainViewModel Main => _configuration.Resolve<MainViewModel>();

        public ZipSelectorViewModel ZipSelector => _zipSelectorViewModel;

        public ZipDetailsViewModel ZipDetails => _zipDetailsViewModel;

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}