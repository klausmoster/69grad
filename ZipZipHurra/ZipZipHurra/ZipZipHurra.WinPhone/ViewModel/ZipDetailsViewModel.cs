using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using ZipZipHurra.DataAndContracts.Contracts;
using ZipZipHurra.DataAndContracts.Models;
using ZipZipHurra.WinPhone.MessengerArgs;
using ZipZipHurra.WinPhone.Models;

namespace ZipZipHurra.WinPhone.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ZipDetailsViewModel : ViewModelBase
    {
        private readonly ICityInfoService _cityInfoService;
        private readonly ICityRepository _cityRepository;
        private City _city;
        private CityInformation _cityInformation;
        private bool _isGettingCityInformation;

        /// <summary>
        /// Name der gew�hlten Stadt.
        /// </summary>
        public City City
        {
            get
            {
                return _city;
            }

            set
            {
                if (_city == value)
                {
                    return;
                }

                _city = value;
                RaisePropertyChanged(nameof(City));
                RaisePropertyChanged(nameof(Title));
            }
        }

        /// <summary>
        /// Informationen �ber die Stadt.
        /// </summary>
        public CityInformation CityInformation
        {
            get
            {
                return _cityInformation;
            }

            set
            {
                if (_cityInformation == value)
                {
                    return;
                }

                _cityInformation = value;
                RaisePropertyChanged(nameof(CityInformation));
            }
        }

        /// <summary>
        /// Stadtinformationen werden geholt.
        /// </summary>
        public bool IsGettingCityInformation
        {
            get
            {
                return _isGettingCityInformation;
            }

            set
            {
                if (_isGettingCityInformation == value)
                {
                    return;
                }

                _isGettingCityInformation = value;
                RaisePropertyChanged(nameof(IsGettingCityInformation));
            }
        }

        public string Title => $"{City.Name} ({City.Zip})";

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public ZipDetailsViewModel(ICityInfoService cityInfoService, ICityRepository cityRepository)
        {
            _cityInfoService = cityInfoService;
            _cityRepository = cityRepository;

            Messenger.Default.Register<ShowCityZipArgs>(this, OnShowCityZip);

            if (IsInDesignMode)
            {
                LoadDesignData();
            }
        }

        private void OnShowCityZip(ShowCityZipArgs showCityZipArgs)
        {
            City = showCityZipArgs.City;
        }

        private void LoadDesignData()
        {
            City = new City
            {
                Name = "Aachen",
                Zip = 1000,
                GeoCoordinate = new GeoCoordinate
                {
                    Latitude = 13.322222,
                    Longitude = 53.293747
                }
            };
            CityInformation = new CityInformation
            {
                Population = 2345736
            };
        }

        private RelayCommand _initCommand;
        public RelayCommand InitCommand => _initCommand
                                           ?? (_initCommand = new RelayCommand(OnInitCommand));

        private void OnInitCommand()
        {
            List<City> nearestCities = _cityRepository.GetNearestFrom(City, 10);

            SetMapLocationArgs setMapLocationArgs = new SetMapLocationArgs(City.GeoCoordinate);
            Messenger.Default.Send(setMapLocationArgs);

            List<MapElement> mapElements = nearestCities.Select(n => new MapElement
            {
                Title = n.Zip.ToString(),
                GeoCoordinate = n.GeoCoordinate
            }).ToList();
            AddMapElementsArgs addMapElementsArgs = new AddMapElementsArgs(mapElements);
            Messenger.Default.Send(addMapElementsArgs);

            IsGettingCityInformation = true;
            _cityInfoService.GetInformation(City.Name).ContinueWith(t =>
            {
                IsGettingCityInformation = false;
                if (t.IsFaulted)
                {
                    
                }

                CityInformation = t.Result;
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}