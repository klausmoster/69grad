using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using ZipZipHurra.DataAndContracts.Models;
using ZipZipHurra.WinPhone.MessengerArgs;

namespace ZipZipHurra.WinPhone.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ZipSelectorViewModel : ViewModelBase
    {
        private City _selectedZip;

        /// <summary>
        /// Name der gew�hlten Stadt.
        /// </summary>
        public City SelectedZip
        {
            get
            {
                return _selectedZip;
            }

            set
            {
                if (_selectedZip == value)
                {
                    return;
                }

                _selectedZip = value;
                RaisePropertyChanged(nameof(SelectedZip));

                ShowCity();
            }
        }

        /// <summary>
        /// �bersicht aller Stadtnamen.
        /// </summary>
        public ObservableCollection<City> Cities { get; set; } = new ObservableCollection<City>();

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public ZipSelectorViewModel()
        {
            Messenger.Default.Register<ShowCityZipsArgs>(this, OnShowCityZips);

            if (IsInDesignMode)
            {
                LoadDesignData();
            }
        }

        private void OnShowCityZips(ShowCityZipsArgs showCityZipsArgs)
        {
            Cities.Clear();
            foreach (City city in showCityZipsArgs.Cities)
            {
                Cities.Add(city);
            }
        }

        private void LoadDesignData()
        {
            Cities.Clear();
            Cities.Add(new City
            {
                Name = "Aachen",
                Zip = 1000
            });
            Cities.Add(new City
            {
                Name = "Aachen",
                Zip = 2000
            });
            Cities.Add(new City
            {
                Name = "Aachen",
                Zip = 3000
            });
        }

        private void ShowCity()
        {
            Messenger.Default.Send(new ShowCityZipArgs(SelectedZip));
        }
    }
}