﻿using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Messaging;
using ZipZipHurra.DataAndContracts.Models;
using ZipZipHurra.WinPhone.MessengerArgs;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace ZipZipHurra.WinPhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : BasePage
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        private void UIElement_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            ListView listView = (ListView) sender;
            var selectedCity = listView.SelectedItem as KeyValuePair<string, List<City>>? ?? new KeyValuePair<string, List<City>>();

            if (selectedCity.Value.Count > 1)
            {
                Messenger.Default.Send(new ShowCityZipsArgs(selectedCity.Value));
            }
            else
            {
                Messenger.Default.Send(new ShowCityZipArgs(selectedCity.Value.First()));
            }
        }
    }
}
