﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace ZipZipHurra.WinPhone.Converter
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            bool visibleWhen = bool.Parse(parameter.ToString());
            bool given = (bool)value;

            if (given == visibleWhen)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
