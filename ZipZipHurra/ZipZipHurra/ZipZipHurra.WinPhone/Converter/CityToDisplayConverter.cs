﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Data;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.WinPhone.Converter
{
    public class CityToDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            KeyValuePair<string, List<City>> keyValuePair = value as KeyValuePair<string, List<City>>? ??
                                                            new KeyValuePair<string, List<City>>();
            if (keyValuePair.Value.Count > 1)
            {
                return $"{keyValuePair.Key} ({keyValuePair.Value.Count})";
            }
            else
            {
                return keyValuePair.Key;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
