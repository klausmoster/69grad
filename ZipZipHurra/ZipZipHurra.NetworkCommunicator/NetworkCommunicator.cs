﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using ZipZipHurra.DataAndContracts.Contracts;

namespace ZipZipHurra.NetworkCommunicator
{
    public class NetworkCommunicator : INetworkCommunicator
    {
        /// <summary>
        /// Lädt den Content der Url.
        /// </summary>
        /// <param name="url">Die Url.</param>
        /// <returns>Der Content.</returns>
        public async Task<string> DownloadString(string url)
        {
            try
            {
                HttpClient httpClient = new HttpClient();
                string content = await httpClient.GetStringAsync(url);
                return content;
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
