﻿using Microsoft.Practices.Unity;
using ZipZipHurra.DataAndContracts.Contracts;

namespace ZipZipHurra.AppConfig
{
    public static class AppConfig
    {
        private static UnityContainer _configuration;

        public static UnityContainer Configuration
        {
            get
            {
                if (_configuration == null)
                {
                    Configure();
                }

                return _configuration;
            }
        }

        private static void Configure()
        {
            _configuration = new UnityContainer();
            _configuration.RegisterType<INetworkCommunicator, NetworkCommunicator.NetworkCommunicator>();
            _configuration.RegisterType<ICityInfoService, CityInfoService.CityInfoService>();
            _configuration.RegisterType<IGeoTools, GeoTools.GeoTools>();
            _configuration.RegisterType<ICityRepository, DataService.CityRepository>();
            _configuration.RegisterType<ITabFileReader, TabFileReader.TabFileReader>();
        }
    }
}
