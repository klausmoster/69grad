﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.TabFileReaderTests
{
    [TestClass]
    public class TabFileReaderTests
    {
        /// <summary>
        /// Die Daten innerhalb der Tab-getrennten Datei müssen nach Ort sortiert ausgelesen werden.
        /// </summary>
        [TestMethod]
        [TestCategory("ZipZipHurra")]
        [TestCategory("TabFileReader")]
        [Owner("Klaus Moster")]
        public void ReadTabFileData()
        {
            //-----------
            // Arrange
            //-----------

            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "ZipZipHurra.TabFileReaderTests.DemoData.TabDemoData.tab";
            TabFileReader.TabFileReader tabFileReader = new TabFileReader.TabFileReader();

            //-----------
            // Act
            //-----------

            Dictionary<string, List<City>> cityDefinitions;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                cityDefinitions = tabFileReader.ReadCityDefinitions(stream);
            }

            //-----------
            // Assert
            //-----------

            Assert.AreEqual(3, cityDefinitions.Keys.Count, "Anzahl Städte");

            Assert.IsTrue(cityDefinitions.ContainsKey("Ort1"), "Ort 1 fehlt");
            Assert.IsTrue(cityDefinitions.ContainsKey("Ort2"), "Ort 2 fehlt");
            Assert.IsTrue(cityDefinitions.ContainsKey("Ort3"), "Ort 3 fehlt");

            // Ort 1
            List<City> ort1 = cityDefinitions["Ort1"];
            Assert.AreEqual(2, ort1.Count, "Ort 1 - Anzahl PLZ");

            City city = ort1.First(o => o.Id == 1);
            Assert.AreEqual(double.Parse("51.0600336463379", CultureInfo.InvariantCulture), city.GeoCoordinate.Latitude, "[Ort 1 | PLZ 1] Latitude");
            Assert.AreEqual(double.Parse("13.7210676148814", CultureInfo.InvariantCulture), city.GeoCoordinate.Longitude, "[Ort 1 | PLZ 1] Longitude");
            Assert.AreEqual("Ort1", city.Name, "[Ort 1 | PLZ 1] Name");
            Assert.AreEqual(10001, city.Zip, "[Ort 1 | PLZ 1] Zip");

            city = ort1.First(o => o.Id == 4);
            Assert.AreEqual(double.Parse("51.0926193047084", CultureInfo.InvariantCulture), city.GeoCoordinate.Latitude, "[Ort 1 | PLZ 2] Latitude");
            Assert.AreEqual(double.Parse("13.8289798683304", CultureInfo.InvariantCulture), city.GeoCoordinate.Longitude, "[Ort 1 | PLZ 2] Longitude");
            Assert.AreEqual("Ort1", city.Name, "[Ort 1 | PLZ 2] Name");
            Assert.AreEqual(10002, city.Zip, "[Ort 1 | PLZ 2] Zip");

            // Ort 2
            List<City> ort2 = cityDefinitions["Ort2"];
            Assert.AreEqual(2, ort2.Count, "Ort 2 - ANzahl PLZ");

            city = ort2.First(o => o.Id == 2);
            Assert.AreEqual(double.Parse("51.0667452412037", CultureInfo.InvariantCulture), city.GeoCoordinate.Latitude, "[Ort 2 | PLZ 1] Latitude");
            Assert.AreEqual(double.Parse("13.7439674110642", CultureInfo.InvariantCulture), city.GeoCoordinate.Longitude, "[Ort 2 | PLZ 1] Longitude");
            Assert.AreEqual("Ort2", city.Name, "[Ort 2 | PLZ 1] Name");
            Assert.AreEqual(20001, city.Zip, "[Ort 2 | PLZ 1] Zip");

            city = ort2.First(o => o.Id == 5);
            Assert.AreEqual(double.Parse("51.1201009324663", CultureInfo.InvariantCulture), city.GeoCoordinate.Latitude, "[Ort 2 | PLZ 2] Latitude");
            Assert.AreEqual(double.Parse("13.7619645364861", CultureInfo.InvariantCulture), city.GeoCoordinate.Longitude, "[Ort 2 | PLZ 2] Longitude");
            Assert.AreEqual("Ort2", city.Name, "[Ort 2 | PLZ 2] Name");
            Assert.AreEqual(20002, city.Zip, "[Ort 2 | PLZ 2] Zip");

            // Ort 3
            List<City> ort3 = cityDefinitions["Ort3"];
            Assert.AreEqual(2, ort3.Count, "Ort 3 - ANzahl PLZ");

            city = ort3.First(o => o.Id == 3);
            Assert.AreEqual(double.Parse("51.0926193047084", CultureInfo.InvariantCulture), city.GeoCoordinate.Latitude, "[Ort 3 | PLZ 1] Latitude");
            Assert.AreEqual(double.Parse("13.8289798683304", CultureInfo.InvariantCulture), city.GeoCoordinate.Longitude, "[Ort 3 | PLZ 1] Longitude");
            Assert.AreEqual("Ort3", city.Name, "[Ort 3 | PLZ 1] Name");
            Assert.AreEqual(30001, city.Zip, "[Ort 3 | PLZ 1] Zip");

            city = ort3.First(o => o.Id == 6);
            Assert.AreEqual(double.Parse("51.0171437482078", CultureInfo.InvariantCulture), city.GeoCoordinate.Latitude, "[Ort 3 | PLZ 2] Latitude");
            Assert.AreEqual(double.Parse("13.7445163387362", CultureInfo.InvariantCulture), city.GeoCoordinate.Longitude, "[Ort 3 | PLZ 2] Longitude");
            Assert.AreEqual("Ort3", city.Name, "[Ort 3 | PLZ 2] Name");
            Assert.AreEqual(30002, city.Zip, "[Ort 3 | PLZ 2] Zip");
        }
    }
}
