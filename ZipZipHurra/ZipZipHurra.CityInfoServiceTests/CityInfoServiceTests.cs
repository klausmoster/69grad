﻿using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZipZipHurra.CityInfoServiceTests.Fakes;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.CityInfoServiceTests
{
    [TestClass]
    public class CityInfoServiceTests
    {
        private readonly CityInfoService.CityInfoService _cityInfoService;
        public TestContext TestContext { get; set; }

        public CityInfoServiceTests()
        {
            FakeNetworkCommunicator fakeNetworkCommunicator = new FakeNetworkCommunicator();
            _cityInfoService = new CityInfoService.CityInfoService(fakeNetworkCommunicator);
        }

        [TestMethod]
        [TestCategory("ZipZipHurra")]
        [TestCategory("CityInfoService")]
        [Owner("Klaus Moster")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
                   "|DataDirectory|\\TestData\\CityInfoData.xml",
                   "Row",
                    DataAccessMethod.Sequential)]
        public void CityInfo()
        {
            //--------------
            // Arrange
            //--------------

            string city = (string)TestContext.DataRow["City"];
            double expectedPopulation = double.Parse((string)TestContext.DataRow["Population"], CultureInfo.InvariantCulture);

            //--------------
            // Act
            //--------------

            ManualResetEvent mre = new ManualResetEvent(false);
            TaskAwaiter<CityInformation> taskAwaiter = _cityInfoService.GetInformation(city).GetAwaiter();
            taskAwaiter.OnCompleted(() =>
            {
                mre.Set();
            });
            mre.WaitOne();
            CityInformation actualCityInformation = taskAwaiter.GetResult();

            //--------------
            // Assert
            //--------------

            Assert.AreEqual(expectedPopulation, actualCityInformation.Population, $"{city}: Population");
        }
    }
}
