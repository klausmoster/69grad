﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ZipZipHurra.DataAndContracts.Contracts;

namespace ZipZipHurra.CityInfoServiceTests.Fakes
{
    public class FakeNetworkCommunicator : INetworkCommunicator
    {
        public async Task<string> DownloadString(string url)
        {
            string city = url.Split('/').ToList().Last();

            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = $"ZipZipHurra.CityInfoServiceTests.TestData.{city}.html";
            string wikiPage;

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    wikiPage = streamReader.ReadToEnd();
                }
            }

            return wikiPage;
        }
    }
}
