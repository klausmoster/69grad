﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZipZipHurra.DataAndContracts.Models;

namespace ZipZipHurra.GeoToolsTests
{
    [TestClass]
    public class GeoToolsTests
    {
        private readonly List<City> _geoPositions = new List<City>
        {
            new City
            {
                Name = "Vorstadt",
                GeoCoordinate = new GeoCoordinate
                {
                    Latitude = 51.0667452412037,
                    Longitude = 13.7439674110642
                }
            },
            new City
            {
                Name = "Friedrichstadt",
                GeoCoordinate = new GeoCoordinate
                {
                    Latitude = 51.0600336463379,
                    Longitude = 13.7210676148814
                }
            },
            new City
            {
                Name = "Altstadt",
                GeoCoordinate = new GeoCoordinate
                {
                    Latitude = 51.039558876083,
                    Longitude = 13.7389066401609
                }
            },
            new City
            {
                Name = "Dresdner Heide",
                GeoCoordinate = new GeoCoordinate
                {
                    Latitude = 51.0926193047084,
                    Longitude = 13.8289798683304
                }
            },
        };
        
        [TestMethod]
        [TestCategory("ZipZipHurra")]
        [TestCategory("GeoTools")]
        [Owner("Klaus Moster")]
        public void GetTheNearestPosition()
        {
            //-----------
            // Arrange
            //-----------

            City vorstadt = _geoPositions.First(g => g.Name == "Vorstadt");
            City friedrichstadt = _geoPositions.First(g => g.Name == "Friedrichstadt");
            List<City> compPositions = _geoPositions.Where(g => g.Name != vorstadt.Name).ToList();
            GeoTools.GeoTools geoTools = new GeoTools.GeoTools();

            //-----------
            // Act
            //-----------

            List<City> actGeoPositions = geoTools.GetNearestCities(vorstadt, compPositions, 2);

            //-----------
            // Assert
            //-----------

            Assert.IsTrue(actGeoPositions.Contains(friedrichstadt), "Friedrichstadt nicht vorhanden");
        }
        
        [TestMethod]
        public void GetTheTwoNearestPositions()
        {
            //-----------
            // Arrange
            //-----------

            City vorstadt = _geoPositions.First(g => g.Name == "Vorstadt");
            City friedrichstadt = _geoPositions.First(g => g.Name == "Friedrichstadt");
            City altstadt = _geoPositions.First(g => g.Name == "Altstadt");
            List<City> compPositions = _geoPositions.Where(g => g.Name != vorstadt.Name).ToList();
            GeoTools.GeoTools geoTools = new GeoTools.GeoTools();

            //-----------
            // Act
            //-----------

            List<City> actGeoPositions = geoTools.GetNearestCities(vorstadt, compPositions, 2);

            //-----------
            // Assert
            //-----------

            Assert.IsTrue(actGeoPositions.Contains(friedrichstadt), "Friedrichstadt nicht vorhanden");
            Assert.IsTrue(actGeoPositions.Contains(altstadt), "Altstadt nicht vorhanden");
        }
    }
}
